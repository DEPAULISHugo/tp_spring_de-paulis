# TP Spring JPA

TP Spring JPA is a Spring application connected to a MySQL Database. It allows to create participants and events and save their relative infomation in a MySQL database.

## Installation

First of all, MySQL must be on your computer. Then, you must initialize the database with the following commands :

1.  Open the MySQL terminal

`sudo mysql`

2. Create the new database

`create database tpspringdepaulis;`

3. Create the new user

`create user 'depaulisuser'@'%' identified by 'password';`

4. Gives all privileges to the new user on the newly created database

`grant all on tpspringdepaulis.* to 'depaulisuser'@'%';`

To run the application, open the project with an IDE such as Eclipse or NetBeans. Select the file TpSpringApplication.java, click on "Run As" and choose the option "Spring Application". You can see the projet running at this address: http://localhost:8080.


## Organisation of the database

The database and the table are automatically created at each start of the server. At the begining, each table is empty and you must fill them thantks to the following page :
 - Gestion des participants
 - Gestion des intervenants
 - Gestion des évènements

### Relation between participants and events

The relation between these two types of element is a many to many relation. A participant can take part to multiple events and an event can accommodate multiple participants.    

To represent this fact, we create a relation table "evenement_participation" that associate a participant and an event with their respective ID.

### Relation between participants and stakeholders

The relation between these two types of element is an inheritance relation. A stakeholder (intervenant) is a special participant that own specific attributes such as the number of hours of his intervention or his specific needs. To resume, a stakeholer is a participant so you can see him in the participants list. However, the contrary is not true, a participant is not a stakeholder.   

To represent this fact, we use the joined strategy. So, we create a new table "intervenant_table" that contains only the specific attributes of a stakeholder. This table is linked to the "participant_table" thanks to the primary key "num_pers".

### Database schema

![alt text](images/tables.png "Database schema")


## Use case

### List objects

The menus "Liste des participants", "Liste des intervenants" et "Liste des évènements" allow the user to see the list of the objects saved in the database.

### Update objets

The menus "Gestion des participants", "Gestion des intervenants" et "Gestion des évènements" allow the user to create, update and delete objects of each type. If you want that a participant or a stakeholder take part to an event, you must have create this event previously.   

WARNING: An event must not host any participant if you want to delete it.
 
