package ensg.eu.TPSpring.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import ensg.eu.TPSpring.metier.Evenement;
import ensg.eu.TPSpring.metier.Participant;
import ensg.eu.TPSpring.services.EvenementRepository;

@Controller
public class EvenementController {
	
	@Autowired
	private EvenementRepository evenementRepository;
	
	@GetMapping("/liste_evenements")
	public String listeEvenements(Model model) {
		
		// Ajout de données au modèle
		model.addAttribute("listeEvenements", evenementRepository.findAll());
			
		//Envoi vers la vue
		return "liste_evenements";
	}
	
	@GetMapping("/gestion_evenements")
	public String gestionEvenements(Model model) {
		
		// Ajout de données au modèle
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		//Envoi vers la vue
		return "gestion_evenements";
	}
	
	@GetMapping("/ajout_evenement")
	public String ajoutEvenement(
			@RequestParam(name="intitule", required=true) String intitule, 
			@RequestParam(name="date_debut", required=true) String date_debut, 
			Model model) {
		
		// Sauvegarde du nouveau participant dans la base de données
		String[] date = date_debut.split("-");
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1]);
		int day = Integer.parseInt(date[2]);
		Evenement newEvenement = new Evenement(intitule, new Date(year, month, day));
		evenementRepository.save(newEvenement);
		
		// Ajout de données au modèle
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		//Envoi vers la vue
		return "gestion_evenements";
	}
	
	@GetMapping(value = "/supprimer_evenement/{id}")
	public String supprimerEvenement(@PathVariable int id, Model model) {
		
		evenementRepository.deleteById(id);
			
		// Ajout de données au modèle
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		//Envoi vers la vue
		return "gestion_evenements";
	}
	
	@GetMapping(value = "/modifier_evenement/{id}")
	public String modifierEvenement(@PathVariable int id, Model model) {
		
		Optional<Evenement> evenement = evenementRepository.findById(id);
		
		if (evenement.isEmpty()) {
			// Ajout de données au modèle
			model.addAttribute("listeEvenements", evenementRepository.findAll());
			
			//Envoi vers la vue
			return "gestion_evenements";
		}
		
		else {
			// Ajout de données au modèle
			model.addAttribute("listeEvenements", evenementRepository.findAll());
			model.addAttribute("evenement", evenement.get());
			
			//Envoi vers la vue
			return "modifier_evenement";
		}

	}
	
	@GetMapping(value = "/sauvegarder_modification_evenement/{idEvenement}")
	public String sauvegarderModification(
			@PathVariable int idEvenement,
			@RequestParam(name="intitule", required=true) String intitule, 
			@RequestParam(name="date_debut", required=true) String date_debut, 
			Model model) {
		
		Optional<Evenement> evenement = evenementRepository.findById(idEvenement);
		
		if (!evenement.isEmpty()) {
			
			// Sauvegarde des modifications dans la base de données
			Evenement evenementModifie = evenement.get();
			
			String[] date = date_debut.split("-");
			int year = Integer.parseInt(date[0]);
			int month = Integer.parseInt(date[1]);
			int day = Integer.parseInt(date[2]);
			
			evenementModifie.intitule = intitule;
			evenementModifie.date_debut = new Date(year, month, day);
			
			evenementRepository.save(evenementModifie);
		}
		
		
		// Ajout de données au modèle
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		//Envoi vers la vue
		return "gestion_evenements";
	}
	
}