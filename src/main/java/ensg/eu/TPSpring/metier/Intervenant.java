package ensg.eu.TPSpring.metier;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "intervenant_table")
@PrimaryKeyJoinColumn(name = "num_pers")
public class Intervenant extends Participant{
	
	@Column(name = "num_inter", nullable = false)
	public int num_inter;
	
	@Column(name = "nombre_heures", nullable = false)
	public int nombreHeures;
	
	@Column(name = "besoins_specifiques", nullable = false)
	public String besoinsSpecifiques;	
	
	public Intervenant(String nom, String prenom, String email, Date date_naiss, int num_inter, int nombresHeures, String besoinsSpecifiques) {
		super(nom, prenom, email, date_naiss);
		this.num_inter = num_inter;
		this.nombreHeures = nombresHeures;
		this.besoinsSpecifiques = besoinsSpecifiques;
	}
	
	public Intervenant() {
		
	}

}
