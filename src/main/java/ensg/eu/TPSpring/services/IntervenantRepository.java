package ensg.eu.TPSpring.services;

import org.springframework.data.repository.CrudRepository;

import ensg.eu.TPSpring.metier.Intervenant;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface IntervenantRepository extends CrudRepository<Intervenant, Integer> {

}